/**
 * Created by chenjz on 2018/5/8.
 */
'use strict'

var express = require('express')
var multiparty = require('multiparty')
var bodyParser = require('body-parser')
var path = require('path')
var fs = require('fs')

var app = express();

app.use(bodyParser({ keepExtensions: true, uploadDir: '/uploads2' }));

app.get('/', function(req, res) {
  res.send('<form action="/uploadFile" method="post" enctype="multipart/form-data"> ' +
      '<input type="file" name="files"> ' +
      '<input type="text" name="username">' +
      '<input type="text" name="password">' +
      '<input type="submit" class="uploadFile" value="文件上传"> ' +
    '</form>')
})

app.post('/uploadFile',function(req, res){
  var msg = {info:'', img:''};

  var uploadPath = __dirname+"/uploads"

  var username = req.body.username
  console.log('post--', username)

  console.log('req.files--', req.files)

  var form = new multiparty.Form();
  form.encoding = 'utf-8';
  form.uploadDir = uploadPath;
  form.maxFilesSize = 2 * 1024 * 1024;    //设置单文件大小限制
  // form.maxFields = 1000;  // 设置所有文件的大小总和

  form.parse(req, function(err, fields, files) {
    console.log('解析图片信息--', err, fields, files)

    console.log('post--', fields.username, fields.password[0])

    if(err){
      msg.info = '上传失败';
      res.send(msg);
      return ;
    }
    // 上传图片的路径
    var uploadFile = files.files[0].path
    // 原文件名称
    var originalFile = uploadPath + files.files[0].originalFilename

    // 重新命名为原文件名，实际的时候不能这样！按照自动生成的来就好了～
    fs.renameSync(uploadFile, originalFile);

    msg.img = originalFile;
    msg.info = '上传成功'
    msg.len = files.length;

    console.log('返回结果--', msg);

    res.send(msg);
  });

});

app.listen(3000);

