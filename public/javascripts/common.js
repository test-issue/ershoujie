/**
 * Created by chenjz on 2018/4/29.
 */
'use strict'

var BASE_URL = '/'

/**
 * 请求后台数据
 *
 * @param module 模块名
 * @param method 方法名
 * @param params 入参
 * @param callback  回调函数
 * @param errback   错误回调
 * @returns {{}}
 */
function ipsAjax (module, method, params, callback, errback) {
  module = module || 'index'
  method = method || 'index'

  params = typeof params === "string" ? params : $.param(params);

  var result = {}     // 返回结果

  $.ajax({
    url: BASE_URL + module + '/' + method,
    type: 'post',
    data: params,
    datatype : "json",
    async: typeof callback !== 'undefined',      // 是否异步
    success: function(response, status){
      if (!response.code) {
        result = response.data
        callback && callback(response.data)
      } else {
        result = response
        errback && errback(response)
      }
    },
    error: function(response, status){
      errback(response)
    }
  });

  return result
}

window.notification = {
  /**
   * 信息提示
   */
  alert: function(_content, callback, _title) {
    var btns = btns || '[确定]';
    this.confirm(_content, callback, _title, btns);
  },

  /**
   * 保存成功的信息提示
   */
  saved: function(_content, callback, _title, btns) {
    _content = _content || '保存数据成功！';
    _title = _title || '操作成功';
    btns = btns || '[返回列表][继续添加]';
    this.confirm(_content, callback, _title, btns);
  },

  /**
   * 成功信息提示
   */
  info: function(_content, callback, _title) {
    _title = _title || '操作成功';
    this.growl(_content, null, _title, null, '#296191');
  },

  /**
   * 成功信息提示
   */
  succeed: function(_content, callback, _title) {
    _title = _title || '操作成功';
    this.growl(_content, null, _title, null, '#659265');
  },

  /**
   * 失败信息提示
   */
  error: function(_content, callback, _title) {
    _title = _title || '操作失败';
    this.growl(_content, null, _title, null, '#C46A69');
  },

  /**
   * 询问信息
   */
  confirm: function(_content, callback, _title, btns) {
    _title = _title || '信息提示';
    btns = btns || '[确定][取消]';
    $.SmartMessageBox({
      title: _title,
      content: _content,
      buttons: btns
    }, callback);
    $('#Msg1').css({
      top: this.scrollHeight + 60
    });
  },
  /**
   * 弹出提示消息
   *
   * @param  {[type]} _content [description]
   * @param  {[type]} _timeout [description]
   * @param  {[type]} _title   [description]
   * @param  {[type]} _icon    [description]
   * @param  {[type]} _color   [description]
   */
  growl: function(_content, _timeout, _title, _icon, _color) {
    _title = _title || '提示消息';
    _color = _color || '#296191';
    _icon = _icon || 'fa fa-bell';
    _timeout = _timeout || 2000;
    $.smallBox({
      sound:false,
      title: _title,
      content: _content,
      color: _color,
      timeout: _timeout,
      icon: _icon
    });
  }
}
