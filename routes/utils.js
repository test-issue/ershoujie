/**
 * Created by chenjz on 2018/4/29.
 */
'use strict'

// var crypto = require('crypto')
var createMd5 = require('./md5')

/**
 * 返回结果到前端界面
 * @param response
 * @param resultObj
 */
var appResponse = function (response, resultObj) {
  response.writeHead(200, {
    'Content-type': 'application/json'
  })
  response.end(JSON.stringify(resultObj))
}

/**
 * 获取随机长度的字符串
 * ---------------------------------------------
 * @param length
 * @returns {string}
 */
var getRandomStr = function (length) {
  length = length || 4
  const chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
  var res = ''

  for (var i = 0; i < length; i++) {
    res += chars.substr(Math.random() * (chars.length - 1), 1)
  }

  return res
}

/**
 * md5加密字符串
 * ---------------------------------------------
 * @param str
 * @returns {string}
 */
// var createMd5 = function (str) {
//   const hash = crypto.createHash('md5')
//   hash.update(str)
//   return hash.digest('hex')
// }

module.exports = {
  appResponse: appResponse,
  getRandomStr: getRandomStr,
  createMd5: createMd5
}