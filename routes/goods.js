/**
 * Created by chenjz on 2018/5/4.
 */
'use strict'

var express = require('express');
var router = express.Router();

var utils = require('./utils')
var multiparty = require('multiparty');
var path = require('path');

/**
 * 发布商品
 */
router.post('/newPublish', function(req, res, next) {

  var result
  if (!req.session.username) {
    console.log('用户未登录。。')
  }
  console.log(req.session.userinfo)

  var uploadDir = '/images/goods/uploads'

  var form = new multiparty.Form()
  form.encoding = 'utf-8';    //设置编辑
  form.uploadDir = path.resolve(__dirname, '..', 'public' + uploadDir)   //设置图片存储路径
  form.keepExtensions = true;   //保留后缀
  form.maxFieldsSize = 2*1024*1024; //内存大小
  form.maxFilesSize = 2*1024*1024;   // 图片限制2M之内

  //表单解析
  form.parse(req, function(error, fields, files) {
    // console.log('参数解析--', error, fields, files)

    var goodsUrl

    if (files) {    // 上传了图片
      //报错处理
      if(error){
        result = {
          code : 2,
          message : '请上传2M以图片'
        };
        utils.appResponse(res, result)
      }

      //获取路径
      var filePath = files.image[0]['path'];
      var suffix = filePath.split('.')[1]

      if (!/(jpg|jpeg|png|gif)/.test(suffix)) {
        result = {
          code : 2,
          message : '请上传正确格式的图片'
        };
        utils.appResponse(res, suffix)
      }

      // 兼容window 文件系统～
      var fileArr = filePath.replace(/\\+/, '/').split('/')
      goodsUrl = uploadDir + '/' + fileArr[fileArr.length-1]
    }

    var classifyId = fields.goodstype[0]

    // goods_image，没有给的时候，默认图片
    if (!goodsUrl) {
      if (['1', '5', '6', '18', '25', '41', '49'].indexOf(classifyId) > -1) {
        goodsUrl = '/images/goods/default_' + classifyId + '.jpg'
      } else {
        goodsUrl = '/images/goods/default.jpg'
      }
    }

    // 发布的商品信息
    var insert = {
      goods_name: fields.goodsname[0],
      goods_classify_id: classifyId,
      goods_detail: fields.content[0],
      goods_price: fields.price[0],
      // goods_number: fields.number[0],
      user_id: req.session.userinfo.id,
      goods_image: goodsUrl
    }


    global.sqlConnection.query("INSERT INTO `wx_goods` SET ? ", insert, function(error, response) {

      console.log('创建新商品--', error, response)

      if (error) {
        result = {
          code: 2,
          message: '数据库创建新商品失败--' + error.message
        }
        utils.appResponse(res, result)
      }

      if (response.insertId) {
        result = {
          code: 0,
          message: '发布商品成功',
          data: response
        }
        utils.appResponse(res, result)
      }
    })

  });

})

/**
 * 撤销商品发布
 */
router.post('/cancelPublish', function(req, res, next) {
  var result

  var goodsId = req.body.goodsId

  global.sqlConnection.query({
    sql: "UPDATE `wx_goods` SET `goods_status` = 2 WHERE `goods_id` = ?",
    values: [ goodsId ]
  }, function(error, response) {
    console.log('撤销商品发布--', error, response)

    if (error) {
      result = {
        code: 2,
        message: '数据库撤销商品失败--' + error.message
      }
      utils.appResponse(res, result)
    }

    if (response.affectedRows > 0) {
      result = {
        code: 0,
        message: '撤销商品发布成功',
        data: response
      }
      utils.appResponse(res, result)
    } else {
      result = {
        code: 2,
        message: '撤销商品发布失败',
        data: response
      }
      utils.appResponse(res, result)
    }
  })
})


/**
 * 商品详情
 */
router.get('/detail', function(req, res, next) {
  var goodsId = req.query.id

  var result

  global.sqlConnection.query({
    sql: 'SELECT * FROM `wx_goods` AS g LEFT JOIN `wx_user` AS u ON u.id = g.user_id WHERE `goods_id` = ?',
    values: [ goodsId ]
  }, function(error, response) {

    console.log('获取商品详情--', error, response)

    if (error) {
      result = {
        code: 2,
        message: '获取商品详情失败--' + error.message
      }
      utils.appResponse(res, result)
    }
    var detail = response[0]

    var statusMap = ['在售', '已售', '下架']
    detail.status = statusMap[detail.goods_status]

    global.sqlConnection.query({
      sql: 'SELECT * FROM `wx_goods_comment` WHERE `comment_goods_id` = ?',
      values: [detail.goods_id]
    }, function(error, response) {

      console.log('获取商品评论--', error, response)

      if (error) {
        result = {
          code: 2,
          message: '获取商品评论失败--' + error.message
        }
        utils.appResponse(res, result)
      }
      var comments = response   // 卖家信息

      var currentUser = req.session.userinfo;

      res.render('detail', { title: '商品详情', detail: detail, comments: comments, currentUser: currentUser});
    })

  })
})


/**
 * 将商品加入购物车
 */
router.post('/addToCart', function (req, res, next) {

  // 商品ID
  var goodsId = req.body.goodsId

  var username = req.session.username

  var result

  // 还没有登录需要先登录！
  if (!username) {
    result = {
      code: 2,
      message: '还没有登录，正跳转到登录界面...'
    }
    utils.appResponse(res, result)
  }

  var userId = req.session.userinfo.id

  var insert = {
    goods_id: goodsId,
    user_id: userId
  }

  // 先检查有没有，如果有的话，直接增加1！

  global.sqlConnection.query({
    sql: 'SELECT * FROM `wx_cart` WHERE deleted = 0 AND goods_id = ? AND user_id = ?',
    values: [goodsId, userId]
  }, function(error, response) {
    if (error) {
      result = {
        code: 2,
        message: '查询是否已加入购物车失败--'+error.message
      }
      utils.appResponse(res, result)
    }

    if (response.length > 0) {

      global.sqlConnection.query({
        sql: 'UPDATE `wx_cart` SET amount = amount + 1 WHERE deleted = 0 AND goods_id = ? AND user_id = ?',
        values: [goodsId, userId]
      }, function(error, response) {
        if (error) {
          result = {
            code: 2,
            message: '加入购物车失败--'+error.message
          }
          utils.appResponse(res, result)
        }

        result = {
          code: 0,
          message: '更新购物车成功',
          data: response
        }
        utils.appResponse(res, result)
      })

    } else {
      global.sqlConnection.query('INSERT INTO `wx_cart` SET ? ', insert, function(error, response) {
        if (error) {
          result = {
            code: 2,
            message: '加入购物车失败--'+error.message
          }
          utils.appResponse(res, result)
        }

        result = {
          code: 0,
          message: '插入购物车成功',
          data: response
        }
        utils.appResponse(res, result)
      })
    }
  })

})

/**
 * 删除购物车商品
 */
router.post('/deleteFromCart', function(req, res, next) {
  var goodsId = req.body.goodsId

  var userid = req.session.userinfo.id

  var result

  global.sqlConnection.query({
    sql: 'UPDATE `wx_cart` SET `deleted`=1 WHERE `goods_id` = ? AND `user_id` = ?',
    values: [goodsId, userid]
  }, function(error, response) {
    if (error) {
      result = {
        code: 2,
        message: '删除购物车商品失败--'+error.message
      }
      utils.appResponse(res, result)
    }

    result = {
      code: 0,
      message: '删除购物车商品成功',
      data: response
    }
    utils.appResponse(res, result)
  })

})

/**
 * 发表评论
 */
router.post('/comment', function(req, res, next) {

  var result

  // 必须校验登录！！
  if (!req.session.username) {
    result = {
      code: 2,
      message: '尚未登录，无法评论'
    }
    utils.appResponse(res, result)
  }

  var userinfo = req.session.userinfo

  var insert = {
    comment_goods_id: req.body.goodsId,
    comment_user_id: userinfo.id,
    comment_username: userinfo.username,
    content: req.body.content,
    createtime: new Date()
  }

  global.sqlConnection.query('INSERT INTO `wx_goods_comment` SET ? ', insert, function(error, response) {
    console.log('创建新评论失败--', error, response)

    if (error) {
      result = {
        code: 2,
        message: '发表评论失败--'+error.message
      }
      utils.appResponse(res, result)
    }

    result = {
      code: 0,
      message: '发表评论成功',
      data: insert
    }
    utils.appResponse(res, result)
  })

})

module.exports = router;
