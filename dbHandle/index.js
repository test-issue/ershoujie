/**
 * Created by chenjz on 2018/4/29.
 */
'use strict'

var mysql = require('mysql');

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '****',            // 数据库密码
  database : 'test_ershoujie'
});

// 连接数据库～
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.log('connected as id ' + connection.threadId);
});

module.exports = connection
